<?php

/**
 * Builds the colorlab settings form.
 */
function colorlab_admin() {
  $form = array();

  $form['colorlab_shopid'] = array(
    '#type' => 'textfield',
    '#title' => t('Your shop id'),
    '#default_value' => variable_get('colorlab_shopid'),
    '#description' => t("The colorlab shopid of your webshop"),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
