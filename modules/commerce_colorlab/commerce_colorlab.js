(function ($) {
  Drupal.behaviors.commerce_colorlab = {
    attach: function (context, settings) {
      console.log(context);
      $('form.colorlab-cart input.form-submit', context).click(function (e) {

        // Keep track of the clicked element.
        var button = $(this);

        // Avoid immediate submit, instead we open the colorlab editor.
        e.preventDefault();

        // Specify callback that gets fired after clicking add to cart in the editor.
        // This is specific for the commerce module.
        opts.callback = function(id, token) {
          Drupal.behaviors.commerce_colorlab.add_to_cart(id, token, button);
        };

        colorlab.open(opts);
      });
    }
  };

  Drupal.behaviors.commerce_colorlab.add_to_cart = function (id, token, button) {
    var form = $(button).parents('form');

    if (form.length) {
      $('input.colorlab-id-field', form).val(id);
      $('input.colorlab-token-field', form).val(token);

      form.submit();
    }
  }
}(jQuery));

