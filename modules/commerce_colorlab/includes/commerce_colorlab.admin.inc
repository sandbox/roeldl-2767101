<?php

/**
 * Builds the colorlab settings form.
 */
function commerce_colorlab_admin() {
  $form = array();

  $content_types = node_type_get_types();
  $content_type_options = array();

  foreach ($content_types as $key => $value) {
    $content_type_options[$key] = $value->name;
  }

  $form['commerce_colorlab_product_content_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Shop content types'),
    '#options' => $content_type_options,
    '#default_value' => variable_get('commerce_colorlab_product_content_types', array()),
    '#description' => t("Node types where customizable products are referenced."),
    '#required' => TRUE,
  );

  $line_item_types = commerce_line_item_types();
  $line_item_type_options = array();

  foreach ($line_item_types as $key => $value) {
    $line_item_type_options[$key] = $value['name'];
  }

  $form['commerce_colorlab_line_item_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Shop line item types'),
    '#options' => $line_item_type_options,
    '#default_value' => variable_get('commerce_colorlab_line_item_types', array()),
    '#description' => t("Line item types where customizable products are referenced."),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
